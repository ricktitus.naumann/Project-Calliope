{-#LANGUAGE OverloadedStrings#-}
{-#LANGUAGE AllowAmbiguousTypes#-}

import Data.IORef
import Data.Text
import Graphics.UI.GLUT hiding (imageWidth, imageHeight, imageData)
import Codec.Picture
import Codec.Picture.Types
import Data.Vector.Storable

main :: IO ()
main = do
  (_progName, _args) <- getArgsAndInitialize
  initialDisplayMode $= [WithDepthBuffer, DoubleBuffered]
  initialWindowSize $= Size 1360 720
  initialWindowPosition $= Position 100 100
  _window <- createWindow "Gameshell Prototype"

  pic <- readImage "../test-assets/adventurer-1.3-Sheet.png"
  let p = case pic of
  	Left x  -> error x
  	Right x -> x
  displayCallback $= display p

  mainLoop

display :: DynamicImage -> DisplayCallback
display pic = do 

  blend $= Enabled
  blendFunc $= (SrcAlpha, OneMinusSrcAlpha)
  clear [ColorBuffer, DepthBuffer]
  loadIdentity

  --rasterPos2i (Vertex2 (325-(div (fromIntegral wi) 2)) (450+(div (fromIntegral hi) 2)))
  pixelZoom $= (1,(-1))
  drawPixels (Size (fromIntegral (dynWidth pic)) (fromIntegral (dynHeight pic))) (PixelData RGBA UnsignedByte (dynamicMap imageData pic))

  swapBuffers

dynWidth :: DynamicImage -> Int
dynWidth img = dynamicMap imageWidth img

dynHeight :: DynamicImage -> Int
dynHeight img = dynamicMap imageHeight img

--dynData :: DynamicImage -> Vector (PixelBaseComponent pixel)
--dynData img = dynamicMap imageData img