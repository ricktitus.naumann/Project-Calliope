module Display where

import Graphics.UI.GLUT
import Graphics.GL.Types
import Data.Char
import Data.IORef
import Data.Maybe (fromJust)
import Codec.Picture as Pict
import Control.Monad
import Control.Concurrent
import Foreign

import String
import Image
import Sound
import Types

type Image = PixelData (Color3 GLubyte)


display :: DisplayList -> IORef [Char] -> IORef [Char] -> IORef Float -> IORef (Float,Float,Float,Float) -> IORef (PixelImage)-> IORef (PixelImage) -> IORef Location -> DisplayCallback
display fontOffset string output cursor backcolor image chara location = do
  str             <- get string
  out             <- get output
  cur             <- get cursor
  (ima,wi,hi,ani,sta) <- get image
  (char,char_wi,char_hi,char_ani,char_sta) <- get chara
  (c1,c2,c3,c4)   <- get backcolor



  blend $= Enabled
  blendFunc $= (SrcAlpha, OneMinusSrcAlpha)
  clearColor $= Color4 c1 c2 c3 c4
  clear [ColorBuffer, DepthBuffer]
  loadIdentity

  let rasterPos2i = rasterPos :: Vertex2 GLint -> IO ()

  -------Background -------------------TODO-

  -------Picture----------------------------  

  when (ima /= [nullPtr]) $ do
    rasterPos2i (Vertex2 (325-(div (fromIntegral wi) 2)) (450+(div (fromIntegral hi) 2)))
    pixelZoom $= (1,(-1))
    drawPixels (Size (fromIntegral wi) (fromIntegral hi)) (PixelData RGBA UnsignedByte (ima !! (ani-2)))


  -------Character Animation----------------

  rasterPos2i (Vertex2 0 300)
  pixelZoom $= (2,2)
  drawPixels (Size (fromIntegral char_wi) (fromIntegral char_hi)) (PixelData RGBA UnsignedByte (char !! (char_ani-2)))

  -------GUI--------------------------------

  color $ Color3 0 0 (0 :: GLfloat) --black

  translate $ Vector3 230 55 (0 :: GLfloat)
  rectangle 200 14
  translate $ Vector3 (-230) (-55) (0 :: GLfloat)

  translate $ Vector3 330 140 (0 :: GLfloat)
  rectangle 300 42
  translate $ Vector3 (-330) (-140) (0 :: GLfloat)

  color $ Color3 1 1 (1 :: GLfloat) --white

  translate $ Vector3 230 55 (0 :: GLfloat)
  rectanglewire 200 14
  translate $ Vector3 (-230) (-55) (0 :: GLfloat)

  translate $ Vector3 330 140 (0 :: GLfloat)
  rectanglewire 300 42
  translate $ Vector3 (-330) (-140) (0 :: GLfloat)


  -------Text-------------------------------

  when (countList out '\n' > 5) (output $~! \o -> (addarr (rmlast (lines o))))

  shadeModel $= Flat
  fontOffset <- makeRasterFont
  rasterPos2i (Vertex2 40 50)
  printString fontOffset str

  let text = lines out
  printText fontOffset text (40,165)

  translate $ Vector3 (40+(cur*10)) (55) (0 :: GLfloat)
  renderPrimitive Lines $ mapM_ vertex3f [(0,8,0), (0,(-8),0)]
  translate $ Vector3 (-40-(cur*10)) (-55) (0 :: GLfloat)

  swapBuffers






vertex3f :: (GLfloat, GLfloat, GLfloat) -> IO ()
vertex3f (x, y, z) = vertex $ Vertex3 x y z

rectanglewire :: GLfloat -> GLfloat -> IO ()
rectanglewire v w = renderPrimitive LineLoop $ mapM_ vertex3f 
  [ ( v, w, 0), ( v,-w, 0), (-v,-w, 0), (-v, w, 0) ]

rectangle :: GLfloat -> GLfloat -> IO ()
rectangle v w = renderPrimitive QuadStrip $ mapM_ vertex3f 
  [ ( v, w, 0), ( v,-w, 0), (-v, w, 0), (-v,-w, 0) ]

cubeSolid :: GLfloat -> IO ()
cubeSolid w = renderPrimitive Quads $ mapM_ vertex3f
  [ ( w, w, w), ( w, w,-w), ( w,-w,-w), ( w,-w, w),
    ( w, w, w), ( w, w,-w), (-w, w,-w), (-w, w, w),
    ( w, w, w), ( w,-w, w), (-w,-w, w), (-w, w, w),
    (-w, w, w), (-w, w,-w), (-w,-w,-w), (-w,-w, w),
    ( w,-w, w), ( w,-w,-w), (-w,-w,-w), (-w,-w, w),
    ( w, w,-w), ( w,-w,-w), (-w,-w,-w), (-w, w,-w) ]

cubeFrame :: GLfloat -> IO ()
cubeFrame w = renderPrimitive Lines $ mapM_ vertex3f
  [ ( w,-w, w), ( w, w, w), ( w, w, w), (-w, w, w), 
    (-w, w, w), (-w,-w, w), (-w,-w, w), ( w,-w, w),
    ( w,-w, w), ( w,-w,-w), ( w, w, w), ( w, w,-w),
    (-w, w, w), (-w, w,-w), (-w,-w, w), (-w,-w,-w),
    ( w,-w,-w), ( w, w,-w), ( w, w,-w), (-w, w,-w),
    (-w, w,-w), (-w,-w,-w), (-w,-w,-w), ( w,-w,-w) ]







idle :: IdleCallback
idle = do

  postRedisplay Nothing






rmlast :: [a] -> [a]
rmlast [] = []
rmlast [x] = []
rmlast (x:xs) = x : rmlast xs

addarr :: [String] -> String
addarr []     = ""
addarr (x:xs) = x ++ "\n" ++ (addarr xs)

countList :: [Char] -> Char -> Int
countList []     a = 0
countList (x:xs) a
  | toLower a == toLower x = 1 + countList xs a
  | otherwise              =     countList xs a






timer :: IORef ([[Char]],Int) -> IORef (PixelImage) -> IORef (PixelImage) -> TimerCallback
timer history image chara = do  
  (h,d)           <- get history
  (ima,wi,he,ani,sta) <- get image
  (char,char_wi,char_he,char_ani,char_sta) <- get chara
  char_idle  <- pathPtrGif "Character/chara_idle.gif" 0

  let gl = Prelude.length ima
  
  let char_gl = Prelude.length char

  when (sta >= 0) $ do
    image $= (ima,wi,he,ani+1,sta)
    when ((gl) < ani) (image $= (ima,wi,he,2,sta))

    when (gl < ani && sta == 1) (image $= ([nullPtr],0,0,2,-1))
    when (gl < ani && sta >  1) (image $= (ima,wi,he,ani,sta-1))
  
  when (char_sta >= 0) $ do
    chara $~! \(i,w,h,a,s) -> (i,w,h,a+1,s)
    when (char_gl < char_ani) (chara $~! \(i,w,h,a,s) -> (i,w,h,2,s))

    when (char_gl < char_ani && char_sta == 1) (chara $= char_idle)
    when (char_gl < char_ani && char_sta >  1) (chara $= (char,char_wi,char_he,2,char_sta-1))

  when (length h > 30) (history $~! \((x:xa:xs),d) -> ((x:xs),d))

  postRedisplay Nothing
  
  addTimerCallback 100 (timer history image chara)











