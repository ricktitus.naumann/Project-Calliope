import Data.IORef
import Data.Text
import Graphics.UI.GLUT
import Foreign

import Bindings
import Menu
import String
import Image
import Sound
import Types
import Locations

main :: IO ()
main = do
  (_progName, _args) <- getArgsAndInitialize
  initialDisplayMode $= [WithDepthBuffer, DoubleBuffered]
  initialWindowSize $= Size 1360 720
  initialWindowPosition $= Position 100 100
  _window <- createWindow "Gameshell Prototype"
  reshapeCallback $= Just reshape
  fontOffset <- myInit
  str        <- newIORef ""
  out        <- newIORef ""
  history    <- newIORef (([""] :: [[Char]]),0)
  cursor     <- newIORef (0 :: Float)
  backcolor  <- newIORef (0,0.1,0.2,0)
  image      <- newIORef ([nullPtr],0,0,2,-1)
  anim       <- newIORef 2
  char       <- newIORef ([nullPtr],0,0,2,-1)

  char_idle  <- pathPtrGif "Character/chara_idle.gif" 0
  char $= char_idle

  (source,device) <- mkSource "Sounds/Kirara Magic - Checkmate - 01 Checkmate.wav"

  location <- newIORef town

  _menu <- attachMenu RightButton (menu device)
  keyboardMouseCallback $= Just (keyboardMouse cursor str out backcolor history image char source device location)
  --idleCallback $= Just idle
  displayCallback $= display fontOffset str out cursor backcolor image char location
  addTimerCallback 100 (timer history image char)
  actionOnWindowClose $= actionClose
  mainLoop


