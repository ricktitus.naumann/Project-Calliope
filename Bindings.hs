module Bindings (idle, reshape, keyboardMouse, display, actionClose, close, timer) where

import Control.Monad (when)
import Graphics.UI.GLUT
import Data.IORef
import System.Exit
import Foreign
import Sound.ALUT

import Types
import Display
import Close
import Aktion



reshape size@(Size w h) = do
   viewport $= (Position 0 0, size)
   matrixMode $= Projection
   loadIdentity
   ortho 0 (fromIntegral w) 0 (fromIntegral h) (-1) 1
   matrixMode $= Modelview 0

press :: IORef Float -> IORef [Char] -> Char -> IO ()
press cursor string char = do
  str <- get string
  cur <- get cursor
  (string $~! \s -> if length s < 38 then (embed s char cur) else s)  >> (cursor $~! \c -> if length str < 38 then (c+1) else c)


keyboardMouse :: IORef Float -> IORef [Char] -> IORef [Char] -> IORef (Float,Float,Float,Float) -> IORef ([[Char]],Int) -> IORef (PixelImage)  -> IORef (PixelImage) -> [Source] -> Device -> IORef Location -> KeyboardMouseCallback
keyboardMouse cursor string output backcolor history image char source device location key Down mod pos = do
  str <- get string
  cur <- (get cursor :: IO Float)
  (c1,c2,c3,c4) <- get backcolor
  (histy,darksty) <- get history

  case key of
    (Char '\ESC') -> close device
    (Char '\DC1') -> close device
    (Char '\b')   -> (string $~! \s -> (backspace cur s)) >> (cursor $~! \c -> if c > 0 then (c-1) else c)
    (Char '\r')   -> when (length str > 0) (action string output backcolor history image char source location)
  
    (Char c) -> press cursor string c -- >> putStrLn (show c)

    (SpecialKey KeyLeft)  -> cursor $~! \c  -> if c>(0 :: Float)                then c-1 else c
    (SpecialKey KeyRight) -> cursor $~! \c  -> if c<(fromIntegral $ length str) then c+1 else c
    (SpecialKey KeyUp)    -> (string $~! \s -> if (length histy) > (darksty+1)  then histy !! (darksty +1) else s) >> (when (darksty /= length histy) (history $~! \(h,d) ->  (h,d+1))) >> when ((length histy) > (darksty+1)) (cursor $~! \c -> fromIntegral (length (histy !! (darksty+1))))
    (SpecialKey KeyDown)  -> (string $~! \s -> if 0 /= darksty                  then histy !! (darksty -1) else s) >> (when (darksty > 0 ) (history $~! \(h,d) ->  (h,d-1))) >> when (0 /= darksty) (cursor $~! \c -> fromIntegral (length (histy !! (darksty-1))))


    (MouseButton LeftButton) -> mouseAction pos
    
    _ -> return ()


keyboardMouse cursor string output backcolor history image char source device location key Up _ _ = do
  str <- get string
  cur <- (get cursor :: IO Float)

  case key of
    (Char '\r') -> (cursor $~! \c -> fromIntegral (length str))

    _ -> return ()

--keyboardMouse _ _ _ _ _ _ = return ()



backspace :: Float -> [a] -> [a]
backspace y []     = []
backspace 1 (x:xs) = backspace 0 xs
backspace y [x] = if y < 1 then [x] else []
backspace y (x:xs) = x : backspace (y-1) xs


embed :: [a] -> a -> Float -> [a]
embed    xs  y 0 = y : xs
embed (x:xs) y z = x : embed (xs) y (z-1)




