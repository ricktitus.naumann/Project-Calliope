module Aktion where

import Text.Read (readMaybe)
import Control.Monad (when)
import Data.IORef
import Data.Char
import Graphics.UI.GLUT
import System.Process
import System.Exit
import Foreign
import System.Random
import Sound.ALUT

import Image
import Sound
import Types
import Locations

import System.Posix.Process
import Control.Concurrent


action :: IORef [Char] -> IORef [Char] -> IORef (Float,Float,Float,Float) -> IORef ([[Char]],Int) -> IORef (PixelImage) -> IORef (PixelImage) -> [Source] -> IORef Location -> IO ()
action string output backcolor history image char source location = do

  str  <- get string
  rng  <- randomRIO (0,100) :: IO (Int)
  char_attack <- pathPtrGif "Character/chara_attack.gif" 1
  char_walk <- pathPtrGif "Character/chara_walk.gif" 7
  loc <- get location

  case length (words str) of  
    1 -> case (words str) !! 0 of

      "red"     -> (backcolor $= (0.5,0,0,0))     >> histyUpdate string history str
      "blue"    -> (backcolor $= (0,0.1,0.2,0))   >> histyUpdate string history str
      "green"   -> (backcolor $= (0,0.5,0,0))     >> histyUpdate string history str
      "black"   -> (backcolor $= (0,0,0,0))       >> histyUpdate string history str
      "white"   -> (backcolor $= (1,1,1,0))       >> histyUpdate string history str
      "david"   -> (backcolor $= (0.9,0.2,0.3,0)) >> histyUpdate string history str

      "rng"     -> (output $= show rng) >> histyUpdate string history str

      "play"    -> (playSound source) >> histyUpdate string history str
      "stop"    -> (stopSound source) >> histyUpdate string history str

      "attack"  -> (char $= char_attack) >> histyUpdate string history str
      "goto"    -> (char $= char_walk) >> histyUpdate string history str

      _         -> bash str string output history >> histyUpdate string history str

    3 -> case (words str) !! 0 of
      x | isNum x -> output $= math str >> histyUpdate string history str

      _           ->  bash str string output history >> histyUpdate string history str

    4 -> case (words str) !! 0 of
      "color"   -> (backcolor $~! \b -> if check (words str) 1 3 then ((arg str 1) / 10, (arg str 2) / 10, (arg str 3) / 10, 0) else b) >> histyUpdate string history str

      _         ->  bash str string output history >> histyUpdate string history str
 
    _         ->  bash str string output history >> histyUpdate string history str



math :: String -> String
math str = case (words str) !! 1 of
  "+" -> genericOp (+) str
  "-" -> genericOp (-) str
  "*" -> genericOp (*) str
  "/" -> genericOp (/) str
  _   -> "Wrong Argument"



mouseAction :: Position -> IO ()
mouseAction pos = return ()



isNum :: String -> Bool
isNum []     = False
isNum [x]    = isDigit x
isNum (x:xs) = isDigit x && isNum xs


histyUpdate :: IORef [Char] -> IORef ([[Char]],Int) -> [Char] -> IO ()
histyUpdate string history str = (history $~! \(h,d) -> (str#h,0)) >> string $~! \s -> ""

f :: IORef String -> (ExitCode, String, String) -> IO ()
f string (exitcode,stdout,stderr) = string $~! \s -> if exitcode == ExitSuccess then stdout else stderr

ar :: String -> String
ar str = merge xs
  where (x:xs) = words str

merge :: [String] -> String
merge []     = ""
merge [x]    = x
merge (x:xs) = x ++ " " ++ merge xs

(#) :: a -> [a] -> [a]
(#) y []     = []
(#) y (x:xs) = x : y : xs

arg :: String -> Int -> Float
arg str x = read ((words str) !! x)

check :: [[Char]] -> Int -> Int -> Bool
check []  y z = False
check str y z
  | z-y < 0  = True
  |otherwise = if length str > z then (readMaybe (str !! y) :: Maybe Float) /= Nothing && check str (y+1) z else False

genericOp :: (Float -> Float -> Float) -> String -> String
genericOp f str = if isNum ((words str) !! 2) then show (f (arg str 0) (arg str 2)) else "ERROR: wrong arguments"


bash :: String -> IORef [Char] -> IORef [Char] -> IORef ([[Char]],Int) -> IO ()
bash str string output history = readCreateProcessWithExitCode (shell str) "" >>= f output >> histyUpdate string history str






