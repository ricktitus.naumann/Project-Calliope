module Close where

import Data.IORef
import Graphics.UI.GLUT
import Sound.ALUT


close :: Device -> CloseCallback
close device = do
  closeDevice device
  leaveMainLoop
  return ()

actionClose :: ActionOnWindowClose
actionClose = Exit
