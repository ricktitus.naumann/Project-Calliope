module Sound where

import Control.Monad
import Sound.ALUT

mkSource :: FilePath -> IO ([Source],Device)
mkSource path = 
  withProgNameAndArgs runALUTUsingCurrentContext $ \_ _ ->
  do
    (Just device) <- openDevice Nothing
    (Just context) <- createContext device []
    currentContext $= Just context
    buffer <- createBuffer (File path)
    [source] <- genObjectNames 1
    queueBuffers source [buffer]
    return ([source], device)

changeSource :: [Source] -> FilePath -> IO ([Source])
changeSource source path = 
  withProgNameAndArgs runALUTUsingCurrentContext $ \_ _ ->
  do
    buffer <- createBuffer (File path)
    [source] <- genObjectNames 1
    queueBuffers source [buffer]
    return [source]

playSound :: [Source] -> IO ()
playSound source =
  withProgNameAndArgs runALUTUsingCurrentContext $ \_ _ ->
  do
    play source
    return ()

stopSound :: [Source] -> IO ()
stopSound source =
  withProgNameAndArgs runALUTUsingCurrentContext $ \_ _ ->
  do
    stop source
    return ()


