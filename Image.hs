module Image (pathPtr,pathPtrGif) where

import Graphics.Rendering.OpenGL hiding (imageHeight)
import Foreign
import Foreign.ForeignPtr.Unsafe
import qualified Codec.Picture.Repa as Repa
import Codec.Picture
import Codec.Picture.Types
import Control.DeepSeq

import Types

-- PixelImage = ([Ptr Word8],Int,Int,Int,Int)
pathPtr :: FilePath -> IO PixelImage
pathPtr path = do  
    img <- Repa.readImage path
    image <- case img of
      Left  x -> error x
      Right x -> return x
    let (dat, w, h) = Repa.toForeignPtr image
    return $ ((unsafeForeignPtrToPtr dat) : [], dynWidth (Repa.imgToImage image), dynHeight (Repa.imgToImage image), 2, -1)

    img <- Repa.readImage path
    let Right image = img
    let (dat, w, h) = Repa.toForeignPtr image
    return $ ((unsafeForeignPtrToPtr dat) : [], dynWidth (Repa.imgToImage image), dynHeight (Repa.imgToImage image), 2, -1)



pathPtrGif :: FilePath -> Int -> IO (PixelImage)
pathPtrGif path reap = do

    g <- readGifImages path
    let Right gif = g
    let dat = map Repa.toForeignPtr (map Repa.convertImage gif)
    return $ (map unsafeForeignPtrToPtr (map fst3 dat), dynWidth (gif !! 0), dynHeight (gif !! 0), 2, reap)


fst3 :: (a,b,c) -> a
fst3 (x,y,z) = x

snd3 :: (a,b,c) -> b
snd3 (x,y,z) = y

thd3 :: (a,b,c) -> c
thd3 (x,y,z) = z

dynWidth :: DynamicImage -> Int
dynWidth img = dynamicMap imageWidth img

dynHeight :: DynamicImage -> Int
dynHeight img = dynamicMap imageHeight img

--dynData :: DynamicImage -> a
--dynData img = dynamicMap imageData img
