module Menu where

import Graphics.UI.GLUT
import Sound.ALUT
import Close



menu device = Menu[menuClose device]


menuClose device = MenuEntry "exit" (menuCallbackClose device)

menuCallbackClose :: Device -> MenuCallback
menuCallbackClose device = close device


