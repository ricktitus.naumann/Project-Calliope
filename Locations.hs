module Locations where

import Types

allLocations :: [Location]
allLocations = [town, plain]

town :: Location 
town = Location { name       = "Town"
                , locations  = [plain,home]
                , background = ""
                , enemys     = []
                , npcs       = [] }


plain :: Location
plain = Location { name       = "Plain"
                 , locations  = [town]
                 , background = ""
                 , enemys     = []
                 , npcs       = [] }


home :: Location
home = Location { name       = "Home"
                , locations  = [town]
                , background = ""
                , enemys     = []
                , npcs       = [] }



townmatrix :: [[Tile]]
townmatrix = [[0,0,0,0,0,0,0,0,0,0],
              [0,0,0,0,0,0,0,0,0,0],
              [0,0,0,0,0,0,0,0,0,0],
              [0,0,0,0,0,0,0,0,0,0],
              [0,0,0,0,0,0,0,0,0,0],
              [0,0,0,0,0,0,0,0,0,0],
              [0,0,0,0,0,0,0,0,0,0],
              [0,0,0,0,0,0,0,0,0,0],
              [0,0,0,0,0,0,0,0,0,0],
              [0,0,0,0,0,0,0,0,0,0]]
             
