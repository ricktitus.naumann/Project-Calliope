{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_Project_Calliope (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/rick/.cabal/bin"
libdir     = "/home/rick/.cabal/lib/x86_64-linux-ghc-8.10.4/Project-Calliope-0.1.0.0-inplace-Project-Calliope"
dynlibdir  = "/home/rick/.cabal/lib/x86_64-linux-ghc-8.10.4"
datadir    = "/home/rick/.cabal/share/x86_64-linux-ghc-8.10.4/Project-Calliope-0.1.0.0"
libexecdir = "/home/rick/.cabal/libexec/x86_64-linux-ghc-8.10.4/Project-Calliope-0.1.0.0"
sysconfdir = "/home/rick/.cabal/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "Project_Calliope_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "Project_Calliope_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "Project_Calliope_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "Project_Calliope_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "Project_Calliope_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "Project_Calliope_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
