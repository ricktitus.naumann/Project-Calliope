module Types where

import Foreign


type PixelImage = ([Ptr Word8],Int,Int,Int,Int)

type Name = String
type Background = FilePath
type Enemy = Int
type NPC = Int

data Location = Location { name :: Name
                         , locations :: [Location]
                         , background :: Background
                         , enemys :: [Enemy]
                         , npcs :: [NPC] }


type Tile = Int
